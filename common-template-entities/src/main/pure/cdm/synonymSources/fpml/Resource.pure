Class cdm::synonymSources::fpml::Resource
{
  {meta::pure::profiles::doc.doc = 'The unique identifier of the resource within the event.'} resourceId: cdm::synonymSources::fpml::ResourceId[1];
  {meta::pure::profiles::doc.doc = 'A description of the type of the resource, e.g. a confirmation.'} resourceType: cdm::synonymSources::fpml::ResourceType[0..1];
  {meta::pure::profiles::doc.doc = 'Indicates the language of the resource, described using the ISO 639-2/T Code.'} language: cdm::synonymSources::fpml::Language[0..1];
  {meta::pure::profiles::doc.doc = 'Indicates the size of the resource in bytes. It could be used by the end user to estimate the download time and storage needs.'} sizeInBytes: Float[0..1];
  {meta::pure::profiles::doc.doc = 'Indicates the length of the resource. For example, if the resource were a PDF file, the length would be in pages.'} length: cdm::synonymSources::fpml::ResourceLength[0..1];
  {meta::pure::profiles::doc.doc = 'Indicates the type of media used to store the content. mimeType is used to determine the software product(s) that can read the content. MIME Types are described in RFC 2046.'} mimeType: cdm::synonymSources::fpml::MimeType[1];
  {meta::pure::profiles::doc.doc = 'The name of the resource.'} name: String[0..1];
  {meta::pure::profiles::doc.doc = 'Any additional comments that are deemed necessary. For example, which software version is required to open the document? Or, how does this resource relate to the others for this event?'} comments: String[0..1];
  string: String[0..1];
}
