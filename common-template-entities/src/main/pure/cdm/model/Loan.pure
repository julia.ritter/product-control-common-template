Class {meta::pure::profiles::doc.doc = 'Identifies a loan by referencing a product identifier and through an optional set of attributes.'} cdm::model::Loan extends cdm::model::ProductBase
{
  {meta::pure::profiles::doc.doc = 'Specifies the borrower. There can be more than one borrower. It is meant to be used in the event that there is no Bloomberg Id or the Secured List isn\'t applicable.'} borrower: cdm::model::LegalEntity[*];
  <<cdm::model::metadata.scheme>> {meta::pure::profiles::doc.doc = 'Specifies the seniority level of the lien.'} lien: String[0..1];
  <<cdm::model::metadata.scheme>> {meta::pure::profiles::doc.doc = 'Specifies the type of loan facility (letter of credit, revolving, ...).'} facilityType: String[0..1];
  {meta::pure::profiles::doc.doc = 'Specifies the credit agreement date is the closing date (the date where the agreement has been signed) for the loans in the credit agreement. Funding of the facilities occurs on (or sometimes a little after) the Credit Agreement date. This underlier attribute is used to help identify which of the company\'s outstanding loans are being referenced by knowing to which credit agreement it belongs. ISDA Standards Terms Supplement term: Date of Original Credit Agreement.'} creditAgreementDate: Date[0..1];
  <<cdm::model::metadata.scheme>> {meta::pure::profiles::doc.doc = 'Denotes the loan tranche that is subject to the derivative transaction. It will typically be referenced as the Bloomberg tranche number. ISDA Standards Terms Supplement term: Bloomberg Tranche Number.'} tranche: String[0..1];
}
