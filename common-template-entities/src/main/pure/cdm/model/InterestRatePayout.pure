Class <<cdm::model::metadata.key>> {meta::pure::profiles::doc.doc = 'A class to specify all of the terms necessary to define and calculate a cash flow based on a fixed, a floating or an inflation index rate. The interest rate payout can be applied to interest rate swaps and FRA (which both have two associated interest rate payouts), credit default swaps (to represent the fee leg when subject to periodic payments) and equity swaps (to represent the funding leg). The associated globalKey denotes the ability to associate a hash value to the InterestRatePayout instantiations for the purpose of model cross-referencing, in support of functionality such as the event effect and the lineage.'} cdm::model::InterestRatePayout extends cdm::model::PayoutBase
[
  InterestRatePayoutChoice: (($this.paymentDates->isEmpty() &&
  $this.paymentDate->isEmpty()) ||
  ($this.paymentDates->isNotEmpty() &&
  $this.paymentDate->isEmpty())) ||
  ($this.paymentDate->isNotEmpty() &&
  $this.paymentDates->isEmpty()),
  FutureValueNotional: if(
  $this.rateSpecification.fixedRate->isEmpty(),
  |$this.payoutQuantity.futureValueNotional->isEmpty(),
  |true
),
  TerminationDate: if(
  $this.payoutQuantity.futureValueNotional->isNotEmpty(),
  |$this.payoutQuantity.futureValueNotional.valueDate ==
    $this.calculationPeriodDates.terminationDate.adjustableDate.adjustedDate,
  |true
),
  FpML_ird_6: if(
  $this.paymentDates.firstPaymentDate->isNotEmpty() &&
    $this.calculationPeriodDates.effectiveDate->isNotEmpty(),
  |$this.paymentDates.firstPaymentDate >
    $this.calculationPeriodDates.effectiveDate.adjustableDate.unadjustedDate,
  |true
),
  FpML_ird_23: if(
  $this.stubPeriod.initialStub->isNotEmpty(),
  |$this.calculationPeriodDates.firstRegularPeriodStartDate->isNotEmpty(),
  |true
),
  FpML_ird_24: if(
  $this.stubPeriod.finalStub->isNotEmpty(),
  |$this.calculationPeriodDates.lastRegularPeriodEndDate->isNotEmpty(),
  |true
),
  InitialStub_FinalStub: if(
  $this.stubPeriod->isNotEmpty(),
  |$this.stubPeriod.initialStub->isNotEmpty() ||
    $this.stubPeriod.finalStub->isNotEmpty(),
  |true
),
  SettlementProvisionSettlementCurrency: if(
  $this.crossCurrencyTerms.settlementProvision.nonDeliverableSettlement->isNotEmpty(),
  |!($this.crossCurrencyTerms.settlementProvision.settlementCurrency ==
    $this.payoutQuantity.quantitySchedule.initialQuantity.unitOfAmount.currency) ||
    !($this.crossCurrencyTerms.settlementProvision.settlementCurrency ==
    $this.payoutQuantity.quantityMultiplier.fxLinkedNotionalSchedule.varyingNotionalCurrency),
  |true
),
  FpML_ird_7_1: if(
  ($this.paymentDates.paymentFrequency.period ==
    $this.calculationPeriodDates.calculationPeriodFrequency.period) &&
    ($this.paymentDates.paymentFrequency.periodMultiplier ==
    $this.calculationPeriodDates.calculationPeriodFrequency.periodMultiplier),
  |$this.compoundingMethod->isEmpty() ||
    ($this.compoundingMethod ==
    cdm::model::CompoundingMethodEnum.None),
  |true
),
  FpML_ird_7_2: if(
  (($this.paymentDates.paymentFrequency.period->isNotEmpty() &&
    $this.calculationPeriodDates.calculationPeriodFrequency.period->isNotEmpty()) &&
    !($this.paymentDates.paymentFrequency.period ==
    $this.calculationPeriodDates.calculationPeriodFrequency.period)) ||
    (($this.paymentDates.paymentFrequency.periodMultiplier->isNotEmpty() &&
    $this.calculationPeriodDates.calculationPeriodFrequency.periodMultiplier->isNotEmpty()) &&
    !($this.paymentDates.paymentFrequency.periodMultiplier ==
    $this.calculationPeriodDates.calculationPeriodFrequency.periodMultiplier)),
  |$this.compoundingMethod->isNotEmpty(),
  |true
),
  FpML_ird_9: if(
  $this.compoundingMethod->isNotEmpty(),
  |$this.resetDates->isNotEmpty(),
  |true
),
  FpML_ird_29: if(
  $this.compoundingMethod->isNotEmpty(),
  |$this.rateSpecification.fixedRate->isEmpty(),
  |true
),
  CalculationPeriodDatesFirstCompoundingPeriodEndDate: if(
  $this.compoundingMethod->isEmpty() ||
    ($this.compoundingMethod ==
    cdm::model::CompoundingMethodEnum.None),
  |$this.calculationPeriodDates.firstCompoundingPeriodEndDate->isEmpty(),
  |true
)
]
{
  {meta::pure::profiles::doc.doc = 'The payer/seller paradigm applies to swap products, whether interest rate swaps or the fee leg of credit default swaps.'} payerReceiver: cdm::model::PayerReceiver[0..1];
  {meta::pure::profiles::doc.doc = 'The specification of the rate value(s) applicable to the contract using either a floating rate calculation, a single fixed rate, a fixed rate schedule, or an inflation rate calculation.'} rateSpecification: cdm::model::RateSpecification[1];
  <<cdm::model::metadata.scheme>> {meta::pure::profiles::doc.doc = 'The day count fraction. The cardinality has been relaxed when compared with the FpML interest rate swap for the purpose of accommodating standardized credit default swaps which DCF is not explicitly stated as part of the economic terms. The data rule InterestRatePayout_dayCountFraction requires that the DCF be stated for interest rate products.'} dayCountFraction: cdm::model::DayCountFractionEnum[0..1];
  {meta::pure::profiles::doc.doc = 'The parameters used to generate the calculation period dates schedule, including the specification of any initial or final stub calculation periods.'} calculationPeriodDates: cdm::model::CalculationPeriodDates[0..1];
  {meta::pure::profiles::doc.doc = 'The payment date schedule, as defined by the parameters that are needed to specify it, either in a parametric way or by reference to another schedule of dates (e.g. the reset dates).'} paymentDates: cdm::model::PaymentDates[0..1];
  {meta::pure::profiles::doc.doc = 'The payment date, where only one date is specified, as for the FRA product.'} paymentDate: cdm::model::AdjustableDate[0..1];
  {meta::pure::profiles::doc.doc = 'Applicable to CDS on MBS to specify whether payment delays are applicable to the fixed Amount. RMBS typically have a payment delay of 5 days between the coupon date of the reference obligation and the payment date of the synthetic swap. CMBS do not, on the other hand, with both payment dates being on the 25th of each month.'} paymentDelay: Boolean[0..1];
  {meta::pure::profiles::doc.doc = 'The reset dates schedule, i.e. the dates on which the new observed index value is applied for each period and the interest rate hence begins to accrue.'} resetDates: cdm::model::ResetDates[0..1];
  {meta::pure::profiles::doc.doc = 'The parameters specifying any discounting conventions that may apply. This element must only be included if discounting applies.'} discountingMethod: cdm::model::DiscountingMethod[0..1];
  {meta::pure::profiles::doc.doc = 'If one or more calculation period contributes to a single payment amount this element specifies whether compounding is applicable and, if so, what compounding method is to be used. This element must only be included when more than one calculation period contributes to a single payment amount.'} compoundingMethod: cdm::model::CompoundingMethodEnum[0..1];
  {meta::pure::profiles::doc.doc = 'The cashflow representation of the swap stream.'} cashflowRepresentation: cdm::model::CashflowRepresentation[0..1];
  {meta::pure::profiles::doc.doc = 'The specification of the principle exchange and settlement provision terms.'} crossCurrencyTerms: cdm::model::CrossCurrencyTerms[0..1];
  {meta::pure::profiles::doc.doc = 'The stub calculation period amount parameters. This element must only be included if there is an initial or final stub calculation period. Even then, it must only be included if either the stub references a different floating rate tenor to the regular calculation periods, or if the stub is calculated as a linear interpolation of two different floating rate tenors, or if a specific stub rate or stub amount has been negotiated.'} stubPeriod: cdm::model::StubPeriod[0..1];
  {meta::pure::profiles::doc.doc = 'Reference to a bond underlier to represent an asset swap or Condition Precedent Bond.'} bondReference: cdm::model::BondReference[0..1];
  {meta::pure::profiles::doc.doc = 'Fixed Amount Calculation'} fixedAmount: Float[0..1];
  {meta::pure::profiles::doc.doc = 'Floating Amount Calculation'} floatingAmount: Float[0..1];
}
