Class {meta::pure::profiles::doc.doc = 'Defines the reset value or fixing value produced in cashflow calculations, during the life-cycle of a financial instrument. The reset process defined in Create_Reset function joins product definition details with observations to compute the reset value.'} cdm::model::Reset
[
  condition1: if(
  $this.observations->size() > 1,
  |$this.aggregationMethodology->isNotEmpty(),
  |true
)
]
{
  {meta::pure::profiles::doc.doc = 'Specifies the reset or fixing value. The fixing value could be a cash price, interest rate, or other value.'} resetValue: cdm::model::Price[1];
  {meta::pure::profiles::doc.doc = 'Specifies the date on which the reset occurred.'} resetDate: Date[1];
  <<cdm::model::metadata.reference>> {meta::pure::profiles::doc.doc = 'Represents an audit of the observations used to produce the reset value. If multiple observations were necessary to produce the reset value, the aggregation method should be defined on the payout.'} observations: cdm::model::Observation[1..*];
  {meta::pure::profiles::doc.doc = 'Identifies the aggregation method to use in the case where multiple observations are used to compute the reset value and the method is not defined in a payout.'} aggregationMethodology: cdm::model::AggregationMethod[0..1];
}
