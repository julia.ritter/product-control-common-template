Class {meta::pure::profiles::doc.doc = 'A class to specify the reference obligation that is associated with a credit derivative instrument.'} cdm::model::ReferenceObligation
[
  LegalEntityChoice: (($this.primaryObligor->isEmpty() &&
  $this.primaryObligorReference->isEmpty()) ||
  ($this.primaryObligor->isNotEmpty() &&
  $this.primaryObligorReference->isEmpty())) ||
  ($this.primaryObligorReference->isNotEmpty() &&
  $this.primaryObligor->isEmpty()),
  AssetChoice: ($this.security->isNotEmpty() &&
  $this.loan->isEmpty()) ||
  ($this.loan->isNotEmpty() &&
  $this.security->isEmpty())
]
{
  {meta::pure::profiles::doc.doc = 'Identifies the underlying asset when it is a security, such as a bond or convertible bond. The security data type requires one or more productIdentifiers, specificaiton of the security type (e.g. debt), and includes optional attributes to specify a debt class, such as asset-backed, as well as seniority.'} security: cdm::model::Security[0..1];
  {meta::pure::profiles::doc.doc = 'Identifies the underlying asset when it is a loan.'} loan: cdm::model::Loan[0..1];
  {meta::pure::profiles::doc.doc = 'The entity primarily responsible for repaying debt to a creditor as a result of borrowing or issuing bonds. ISDA 2003 Term: Primary Obligor.'} primaryObligor: cdm::model::LegalEntity[0..1];
  <<cdm::model::metadata.reference>> {meta::pure::profiles::doc.doc = 'A pointer style reference to a reference entity defined elsewhere in the document. Used when the reference entity is the primary obligor.'} primaryObligorReference: cdm::model::LegalEntity[0..1];
  {meta::pure::profiles::doc.doc = 'The party that guarantees by way of a contractual arrangement to pay the debts of an obligor if the obligor is unable to make the required payments itself. ISDA 2003 Term: Guarantor.'} guarantor: cdm::model::LegalEntity[0..1];
  {meta::pure::profiles::doc.doc = 'A pointer style reference to a reference entity defined elsewhere in the document. Used when the reference entity is the guarantor.'} guarantorReference: String[0..1];
  {meta::pure::profiles::doc.doc = 'Indicates if the reference obligation is a Standard Reference Obligation. ISDA 2014 Term: Standard Reference Obligation.'} standardReferenceObligation: Boolean[0..1];
}
