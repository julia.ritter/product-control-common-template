Class {meta::pure::profiles::doc.doc = 'A data that:  is extending the Offset structure for providing the ability to specify an FX fixing date as an offset to dates specified somewhere else in the document.'} cdm::model::FxFixingDate extends cdm::model::Offset
[
  BusinessCentersChoice: (($this.businessCenters->isEmpty() &&
  $this.businessCentersReference->isEmpty()) ||
  ($this.businessCenters->isNotEmpty() &&
  $this.businessCentersReference->isEmpty())) ||
  ($this.businessCentersReference->isNotEmpty() &&
  $this.businessCenters->isEmpty()),
  DateRelativeChoice: ($this.dateRelativeToPaymentDates->isNotEmpty() &&
  $this.dateRelativeToCalculationPeriodDates->isEmpty()) ||
  ($this.dateRelativeToCalculationPeriodDates->isNotEmpty() &&
  $this.dateRelativeToPaymentDates->isEmpty())
]
{
  {meta::pure::profiles::doc.doc = 'The convention for adjusting a date if it would otherwise fall on a day that is not a business day, as specified by an ISDA convention (e.g. Following, Precedent).'} businessDayConvention: cdm::model::BusinessDayConventionEnum[1];
  businessCenters: cdm::model::BusinessCenters[0..1];
  <<cdm::model::metadata.reference>> {meta::pure::profiles::doc.doc = 'A reference to a set of financial business centers defined elsewhere in the document. This set of business centers is used to determine whether a particular day is a business day or not.'} businessCentersReference: cdm::model::BusinessCenters[0..1];
  {meta::pure::profiles::doc.doc = 'The payment date references on which settlements in non-deliverable currency are due and will then have to be converted according to the terms specified through the other parts of the nonDeliverableSettlement structure.'} dateRelativeToPaymentDates: cdm::model::DateRelativeToPaymentDates[0..1];
  {meta::pure::profiles::doc.doc = 'The calculation period references on which settlements in non-deliverable currency are due and will then have to be converted according to the terms specified through the other parts of the nonDeliverableSettlement structure. Implemented for Brazilian-CDI swaps where it will refer to the termination date of the appropriate leg.'} dateRelativeToCalculationPeriodDates: cdm::model::DateRelativeToCalculationPeriodDates[0..1];
}
