[![FINOS - Forming](https://cdn.jsdelivr.net/gh/finos/contrib-toolbox@master/images/badge-forming.svg)](https://github.com/finos/community/blob/master/governance/Software-Projects/Project-Lifecycle.md#forming-projects-optional)

# Product Control Common Template Standard

Construct a common template data model using Legend Studio (an approved open sourced data modelling tool that allow users to create data model in a graphical user interface).

## Background 

## Roadmap

To do: List the roadmap steps; alternatively link the Confluence Wiki page where the project roadmap is published.

1. Item 1
2. Item 2
3. ....

# Get Involved: Contribute to the Product Control Common Template standard

There are several ways to contribute to the Product Control Common Template standard:

* **Join the next meeting**: 

To do: Set meetings

* **Join the mailing list**: Communications for the Product Control Common Template standard are conducted through the product-control-common-template@finos.org mailing list. Please email [product-control-common-template@finos.org](mailto:product-control-common-template@finos.org) to join the mailing list.

* **Raise an issue**: if you have any questions or suggestions, please [raise an issue](https://gitlab.com/finosfoundation/legend/financial-objects/product-control-common-template/-/issues?sort=created_date&state=opened)

## License

This project uses the **Community Specification License 1.0** ; you can read more in the [LICENSE](LICENSE) file.
